# Borger

A script for home folder backups using [Borg](https://borgbackup.readthedocs.io).

## Features

* Multiple destinations using config files.
* Automatically initializes repository in the remote destination.
* Automatically pruning of old backups.

## Usage

Create a config for your `servername` destination at `~/.config/borger/servername`:

    # Backup destination
    export SSH_SERVER="user@host"
    export SSH_PORT="2202"

    # Repository path
    export BORG_REPO_DIR="/var/backups/users/$USER/borg"
    export BORG_REPO="ssh://$SSH_SERVER:$SSH_PORT/$BORG_REPO_DIR"

    # Setting one of those, so you won't be asked for your repository passphrase:
    #export BORG_PASSPHRASE='HACKME'
    #export BORG_PASSCOMMAND='pass show backup'
    #export BORG_PASSCOMMAND='keyringer default decrypt borg 2> /dev/null'

Then run borger:

    borger servername

If you want to backup to local folder or a locally-mounted USB disk, use the
following config at `~/.config/borger/my-disk`:

    # Repository path
    export BORG_REPO="/media/my-disk/backups/users/$USER/borg"

    # Setting one of those, so you won't be asked for your repository passphrase:
    #export BORG_PASSPHRASE='HACKME'
    #export BORG_PASSCOMMAND='pass show backup'
    #export BORG_PASSCOMMAND='keyringer default decrypt borg 2> /dev/null'

Then run borger normally:

    borger my-disk

Note that `BORGER_REPO` param assumes that the path is available. If this path represents
a mountpoint for an external drive then you should mount it manually.

## Optional config

These include:

    # Optional backup config
    export KEEPDAILY="7"         # how many days   to keep
    export KEEPWEEKLY="4"        # how many weeks  to keep
    export KEEPMONTHLY="6"       # how many months to keep
    export ENCRYPTION="keyfile"  # encryption strategy, PAY ATTENTION TO THIS
    export PLACEHOLDER="{user}"  # placeholder to tag archives
    export INTERVAL="1h"         # interval between backups in the continuous mode

## Continuous backups

If you want to run your backups continuously, use

    borger servername --continuous

By default `borger` waits `1h` before starting the new backup procedure which
can be adjusted using the `INTERVAL` config variable. See [this
issue](https://github.com/borgbackup/borg/issues/325) for a discussion on
continuous backups.

## Listing your backups

As simply as

    borger servername --list
    borger mydisk     --list

## Multiple configuration profiles

You can process multiple backup destinations in the same command by placing each
configuration in a subfolder of `~/.config/borger`, like `~/.config/borger/default/servername`
and `~/.config/borger/default/mydisk` and invoking both using commands like

    borger default
    borger default --list
    borger default --continuous

Each config could even be a symbolic links to other configs, so you can build different
backup profiles for different situations, eg. when your laptop is at home, office or if you're
backing up to multiple external drives at the same time.

## WARNING

Borger uses `keyfile` encrytion by default. That means **you should backup your keyfile**
somewhere else. **If you don't want that, use `encryption="repokey"` at your config.**

See [Repository Encryption](https://borgbackup.readthedocs.io/en/stable/quickstart.html#repository-encryption)
for details.

## Known issues

Borger needs a better lock/session management to prevent errors like

    Failed to create/acquire the lock (timeout)

See also:

* `borg break-lock` (before 1.1.0 in theory)
* https://www.solved.tips/failed-create-acquire-lock/
* https://github.com/rugk/borg-cron-helper
* https://github.com/rugk/borg-cron-helper#local-lock-borg--v110
* https://github.com/borgbackup/borg/issues/2306
* https://github.com/borgbackup/borg/issues/813
* https://github.com/borgbackup/borg/issues/2306
* https://github.com/borgbackup/borg/issues/3191
